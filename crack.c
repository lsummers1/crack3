#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be
const int HASH_LEN=33;        // MD5 hash length

// Stucture to hold both a plaintext password and a hash.
struct entry
{
    char *pass;
    char *hash;
};

// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    FILE *fp = fopen(filename, "r");

    *size = 0;
    char line[PASS_LEN];
    struct entry *dic = calloc(1, sizeof(struct entry));

    while(fgets(line, PASS_LEN, fp))
    {
        line[strlen(line)-1] = '\0';
        char *pass = calloc(strlen(line), sizeof(char));
        strcpy(pass, line);
        dic = realloc(dic, sizeof(struct entry) * (*size+1));
        dic[*size].pass = pass;
        dic[*size].hash = md5(line, strlen(line));
        *size += 1;
    }
    fclose(fp);
    return dic;
}

// qsort compare function
int qcomp(const void *a, const void *b)
{
    struct entry x = *(struct entry*)a;
    struct entry y = *(struct entry*)b;
    return (strcmp(x.hash, y.hash));
}

// bsearch compare function
int bcomp(const void *a, const void *b)
{
    struct entry y = *(struct entry*)b;
    return (strcmp((char*)a, y.hash));
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of entry structures
    int size;
    struct entry *dict = read_dictionary(argv[2], &size);

    // Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, size, sizeof(struct entry), qcomp);

    /* // Partial example of using bsearch, for testing. Delete this line */
    /* // once you get the remainder of the program working. */
    /* // This is the hash for "rockyou". */
    /* struct entry *found = (struct entry*)bsearch("f806fc5a2a0d5ba2471600758452799c", dict, size-1, sizeof(struct entry), bcomp); */
    /* printf("%s\n", found->pass); */

    // Open the hash file for reading.
    FILE *fp = fopen(argv[1], "r");

    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char hash[HASH_LEN+1];
    while(fgets(hash, HASH_LEN+1, fp))
    {
        hash[strlen(hash)-1] = '\0';
        struct entry *found = (struct entry*)bsearch(hash, dict, size, sizeof(struct entry), bcomp);
        printf("%s %s\n", found->hash, found->pass);
    }
    free(dict);
    fclose(fp);
}
